package org.test.locators;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SauceLabsLocators {
    public static String usernameLocator = "[data-test=\"username\"]";
    public static String passwordLocator = "[data-test=\"password\"]";
    public static String loginButtonLocator = "[data-test=\"login-button\"]";
    public static String openMenuLocator = "Open Menu";
    public static String sauceLabsMochila = "item_4_title_link";
    public static String sauceLabsMochilaAddCart = "[data-test=\"add-to-cart-sauce-labs-backpack\"]";
    public static String backToProducts = "[data-test=\"back-to-products\"]";
    public static String sauceLabsBuzo = "item_5_title_link";
    public static String sauceLbsBuzoAddCart = "[data-test=\"add-to-cart-sauce-labs-fleece-jacket\"]";
    public static String carrito ="[class='shopping_cart_link']";
    public static String inventario = "#inventory_container";

}
