package org.test.config;
import org.junit.After;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


// ...

public class Config {
    public WebDriver driver;

    public void launchSelenium(String browserName, String headless) {
        ChromeOptions options = new ChromeOptions();

        switch (browserName.toLowerCase()) {
            case "chrome":
                System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver/chromedriver.exe");
                options.addArguments("ignore-certificate-errors");
                driver = new ChromeDriver(options);
                break;

            case "msedge":

                driver = new EdgeDriver();
                break;

            case "firefox":
                driver = new FirefoxDriver();
                break;

            default:
                throw new IllegalArgumentException("Unsupported browser: " + browserName);
        }
        driver.manage().window().maximize();
    }
    public void launchApplication(String url) {
        driver.navigate().to(url);
        WebDriverWait wait = new WebDriverWait(driver, 10);
        //WebElement elementoEsperado = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("id_del_elemento")));

    }
    @After
    public void closeSelenium() {
            driver.close();
            driver.quit();
    }
}
