package org.test.page;

import org.test.hooks.Hooks;
import org.test.locators.SauceLabsLocators;

import java.io.FileInputStream;
import java.util.Properties;

public class ActionPage extends Hooks {
    public String[] checkEnviroment() {
        String ambiente = System.getenv("AMBIENTE");
        Properties prop = new Properties();
        String user = "";
        String pass = "";
        try (FileInputStream data = new FileInputStream("src/test/resources/test.properties")) {
            prop.load(data);
            if (ambiente.equalsIgnoreCase("qa") || ambiente.equalsIgnoreCase("QA")) {
                user = prop.getProperty("usuario.qa");
                pass = prop.getProperty("password.qa");

            } else if(ambiente.equalsIgnoreCase("DEV") || ambiente.equalsIgnoreCase("dev")) {
                user = prop.getProperty("usuario.dev");
                pass = prop.getProperty("password.dev");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return new String[]{user,pass};
    }
    public void login (String user, String pass){
        clickBycssSelector(SauceLabsLocators.usernameLocator);
        writeFieldCssSelector(SauceLabsLocators.usernameLocator,user);
        clickBycssSelector(SauceLabsLocators.passwordLocator);
        writeFieldCssSelector(SauceLabsLocators.passwordLocator,pass);
        clickBycssSelector(SauceLabsLocators.loginButtonLocator);
    }

    public void goToMenuAndItems(){
        //checkElementById(SauceLabsLocators.inventario);
        System.out.println("AAAAAAAAAAAAAAAAAAAAAAA");
    }

    public void selectMochilaItem(){
        clickById(SauceLabsLocators.sauceLabsMochila);
        clickBycssSelector(SauceLabsLocators.sauceLabsMochilaAddCart);
        clickBycssSelector(SauceLabsLocators.backToProducts);

    }

    public void selectBuzoItem(){
        clickById(SauceLabsLocators.sauceLabsBuzo);
        clickBycssSelector(SauceLabsLocators.sauceLbsBuzoAddCart);
        clickBycssSelector(SauceLabsLocators.backToProducts);
    }

    public void goToCarrito(){
        clickBycssSelector(SauceLabsLocators.carrito);
    }

    public String[] returnPrices(){
        String[] precios = new String[2];
        precios[0] = getTextByText("29.99");
        precios[1] = getTextByText("49.99");
        return precios;
    }



}
