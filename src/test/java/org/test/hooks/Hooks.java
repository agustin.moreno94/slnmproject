package org.test.hooks;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.test.config.Config;

import java.io.FileInputStream;
import java.util.Properties;

public class Hooks {
    Config config = new Config();

    @BeforeEach
    public void browserStart() {
        Properties prop = new Properties();
        try (FileInputStream data = new FileInputStream("src/test/resources/test.properties")) {
            prop.load(data);
            String ambiente = System.getenv("AMBIENTE");
            String navegador = System.getenv("NAVEGADOR");
            String url = "";
            String browser = "";

            if (ambiente.equalsIgnoreCase("qa") || ambiente.equalsIgnoreCase("QA")) {
                url = prop.getProperty("url.qa");
                System.out.println(prop.getProperty("url.qa"));


            } else if (ambiente.equalsIgnoreCase("DEV") || ambiente.equalsIgnoreCase("dev")) {
                url = prop.getProperty("url.dev");

            }

            if (navegador.equalsIgnoreCase("chromium")) {
                browser = "chromium";
            }
            if (navegador.equalsIgnoreCase("chrome")) {
                browser = "chrome";
            }
            String headless = "false";
            config.launchSelenium(browser, headless);
            config.launchApplication(url);
        } catch (Exception e) {
            config.closeSelenium();
            e.printStackTrace();
        }

    }

    @AfterEach
    public void closeSelenium() {
        config.closeSelenium();
    }
    //////////////////////METODOS ACCIONES//////////////////////////////

    public void clickBycssSelector(String selector) {
        WebDriverWait wait = new WebDriverWait(config.driver, 10);
        WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(selector)));
        element.click();
    }

    public void clickByXpath(String xPathSelector) {
        WebDriverWait wait = new WebDriverWait(config.driver, 10);
        WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xPathSelector)));
        element.click();
    }

    public void clickById(String iDSelector) {
        // Localiza el elemento por su ID
        WebElement element = config.driver.findElement(By.id(iDSelector));

        // Realiza clic en el elemento
        element.click();
    }
    public void clickByText(String nameElementXpath){
        String nameElementByXpath = "//*[text()='" +nameElementXpath+"']";
        WebDriverWait wait = new WebDriverWait(config.driver, 10);
        WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(nameElementByXpath)));
        WebElement elementByRelativeXPath = config.driver.findElement(By.xpath(nameElementByXpath));
        elementByRelativeXPath.click();
    }

    public String getTextByText(String nameElementXpath){
        String nameElementByXpath = "//*[text()='" +nameElementXpath+"']";
        WebDriverWait wait = new WebDriverWait(config.driver, 10);
        WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(nameElementByXpath)));
        return element.getText();
    }

    public void writeFieldCssSelector(String selector, String text) {
        WebDriverWait wait = new WebDriverWait(config.driver, 10);
        WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(selector)));
        element.sendKeys(text);
    }

    public void writeFieldById(String Idselector, String text) {
        WebDriverWait wait = new WebDriverWait(config.driver, 10);
        WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(Idselector)));
        element.sendKeys(text);
    }

    public void writeFieldByxPath(String xpathSelector, String text) {
        WebDriverWait wait = new WebDriverWait(config.driver, 10);
        WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpathSelector)));
        element.sendKeys(text);
    }

    public String getTextById(String selectorId){
        WebDriverWait wait = new WebDriverWait(config.driver, 10);
        WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(selectorId)));
        return element.getText();
    }
    public String getTextByCssSelector(String cssSelector){
        WebDriverWait wait = new WebDriverWait(config.driver, 10);
        WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(cssSelector)));
        return element.getText();
    }

    public String getTextByXPath(String xPathSelector){
        WebDriverWait wait = new WebDriverWait(config.driver, 10);
        WebElement element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xPathSelector)));
        return element.getText();
    }

    public void checkElementById (String idSelector){
        WebElement element = config.driver.findElement(By.id(idSelector));
        WebDriverWait wait = new WebDriverWait(config.driver, 10);
        wait.until(ExpectedConditions.visibilityOf(element));

    }
}