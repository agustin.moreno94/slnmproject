package org.test.steps;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Step;
import org.junit.Assert;
import org.test.page.ActionPage;


@Epic("Epic Name")
@Feature("Feature Name")

public class SauceLabsSteps {
    ActionPage actionPage = new ActionPage();

    @Given("que soy un {string} y {string} standard")
    @Description("Description of the standard")
    @Step("Step description for the standard")
    public void que_soy_un_y_standard(String user, String pass) {
        actionPage.browserStart();
        actionPage.login(user,pass);
    }
    @When("compro un buzo y una mochila")
    @Description("Description of the mochila")
    @Step("Step description for the mochila")
    public void compro_un_buzo_y_una_mochila()  {
        actionPage.goToMenuAndItems();
        actionPage.selectMochilaItem();
        actionPage.selectBuzoItem();
    }

    @Then("reviso los precios en el carrito")
    @Description("Description of the carrito")
    @Step("Step description for the carrito ")
    public void reviso_los_precios_en_el_carrito()  {

        actionPage.goToCarrito();
        String[] prices = actionPage.returnPrices();

        Assert.assertEquals("$29.99",prices[0]);
        Assert.assertEquals("$49.99",prices[1]);

        actionPage.closeSelenium();

    }

}
